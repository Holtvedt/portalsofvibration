// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "POVGameInstanceBase.generated.h"

USTRUCT(BlueprintType)
struct FmapInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FString MapURL;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FString MapName;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FString MapDescription;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FString MaxPlayers;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		class UTexture2D* MapImage;
};

UCLASS()
class PORTALSOFVIBRATION_API UPOVGameInstanceBase : public UGameInstance
{
	GENERATED_BODY()
public:
	UPOVGameInstanceBase();

protected:
	UFUNCTION(BlueprintCallable)
		void GoToMap(FString MAPURL);
};
