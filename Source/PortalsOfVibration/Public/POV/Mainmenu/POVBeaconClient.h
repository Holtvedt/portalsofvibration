// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OnlineBeaconClient.h"
#include "PortalsOfVibration/Public/POV/Mainmenu/POVBeaconHostObject.h"
#include "POVBeaconClient.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FConnectSuccess, bool, FOnConnected);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDisconnected);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FLobbyUpdated, FPOVLobbyInfo, FOnLobbyUpdated);

UCLASS()
class PORTALSOFVIBRATION_API APOVBeaconClient : public AOnlineBeaconClient
{
	GENERATED_BODY()
		
public:
	APOVBeaconClient();

protected:
	UPROPERTY(BlueprintAssignable)
		FConnectSuccess FOnConnected;

	UPROPERTY(BlueprintAssignable)
		FDisconnected FOnDisconnected;

	UPROPERTY(BlueprintAssignable)
		FLobbyUpdated FOnLobbyUpdated;

	uint8 PlayerIndex;

protected:
	UFUNCTION(BlueprintCallable)
		bool ConnectToServer(const FString& Address);

	UFUNCTION(BlueprintCallable)
		void LeaveLobby();

	virtual void OnFailure() override;
	virtual void OnConnected() override;

public:
	UFUNCTION(Client, Reliable)
		void Client_OnDisconnected();
	virtual void Client_OnDisconnected_Implementation();

	UFUNCTION(Client, Reliable)
		void Client_OnLobbyUpdated(FPOVLobbyInfo LobbyInfo);
	void Client_OnLobbyUpdated_Implementation(FPOVLobbyInfo LobbyInfo);

	void SetPlayerIndex(uint8 Index);

	uint8 GetPlayerIndex();
};
