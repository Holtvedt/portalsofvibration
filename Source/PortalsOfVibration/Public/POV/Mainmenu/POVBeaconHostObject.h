// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OnlineBeaconHostObject.h"
#include "POVBeaconHostObject.generated.h"

USTRUCT(BlueprintType)
struct FPOVLobbyInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite)
		class UTexture2D* MapImage;

	UPROPERTY(BlueprintReadOnly)
		TArray<FString> PlayerList;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHostLobbyUpdated, FPOVLobbyInfo, FOnHostLobbyUpdated);

UCLASS()
class PORTALSOFVIBRATION_API APOVBeaconHostObject : public AOnlineBeaconHostObject
{
	GENERATED_BODY()
public:
	APOVBeaconHostObject();

protected:
	FPOVLobbyInfo LobbyInfo;

	UPROPERTY(BlueprintAssignable)
		FHostLobbyUpdated FOnHostLobbyUpdated;

	UFUNCTION(BlueprintCallable)
		void UpdateLobbyInfo(FPOVLobbyInfo NewLobbyInfo);

	void UpdateClientLobbyInfo();

protected:
	virtual void BeginPlay() override;

	virtual void OnClientConnected(AOnlineBeaconClient* NewClientActor, UNetConnection* ClientConnection) override;
	virtual void NotifyClientDisconnected(AOnlineBeaconClient* LeavingClientActor) override;

	UFUNCTION(BlueprintCallable)
		void ShutdownServer();

	void DisconnectAllClients();

	virtual void DisconnectClient(AOnlineBeaconClient* ClientActor) override;
};
