// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PortalsOfVibrationGameMode.generated.h"

UCLASS(minimalapi)
class APortalsOfVibrationGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APortalsOfVibrationGameMode();
};



