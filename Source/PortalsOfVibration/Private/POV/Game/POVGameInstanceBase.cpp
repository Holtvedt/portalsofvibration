// Fill out your copyright notice in the Description page of Project Settings.


#include "PortalsOfVibration/Public/POV/Game/POVGameInstanceBase.h"

#include "Engine/World.h"

UPOVGameInstanceBase::UPOVGameInstanceBase()
{

}

void UPOVGameInstanceBase::GoToMap(FString MAPURL)
{
	GetWorld()->ServerTravel(MAPURL);
}
