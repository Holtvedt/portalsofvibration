// Fill out your copyright notice in the Description page of Project Settings.


#include "PortalsOfVibration/Public/POV/Mainmenu/MainMenuGameMode.h"
#include "PortalsOfVibration/Public/POV/Mainmenu/POVBeaconHostObject.h"

#include "OnlineBeaconHost.h"

AMainMenuGameMode::AMainMenuGameMode()
{
    HostObject = nullptr;
    Host = nullptr;
}

bool AMainMenuGameMode::CreateHostBeacon()
{
    Host = GetWorld()->SpawnActor<AOnlineBeaconHost>(AOnlineBeaconHost::StaticClass());
    if (Host)
    {
        UE_LOG(LogTemp, Warning, TEXT("SPAWNED AOnlineBeaconHost"));
        if (Host->InitHost())
        {
            UE_LOG(LogTemp, Warning, TEXT("INIT HOST"));
            Host->PauseBeaconRequests(false);

            HostObject = GetWorld()->SpawnActor<APOVBeaconHostObject>(APOVBeaconHostObject::StaticClass());
            if (HostObject)
            {
                UE_LOG(LogTemp, Warning, TEXT("SPAWNED HOST OBJECT"));
                Host->RegisterHost(HostObject);
                return true;
            }
        }
    }
    return false;
}

APOVBeaconHostObject* AMainMenuGameMode::GetBeaconHost()
{
    return HostObject;
}
