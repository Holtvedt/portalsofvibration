// Fill out your copyright notice in the Description page of Project Settings.


#include "PortalsOfVibration/Public/POV/Mainmenu/POVBeaconHostObject.h"
#include "PortalsOfVibration/Public/POV/Mainmenu/POVBeaconClient.h"
#include "PortalsOfVibration/Public/POV/Mainmenu/MainMenuGameMode.h"
#include "OnlineBeaconHost.h"

APOVBeaconHostObject::APOVBeaconHostObject()
{
	ClientBeaconActorClass = APOVBeaconClient::StaticClass();
	BeaconTypeName = ClientBeaconActorClass->GetName();
}

void APOVBeaconHostObject::UpdateLobbyInfo(FPOVLobbyInfo NewLobbyInfo)
{
	LobbyInfo.MapImage = NewLobbyInfo.MapImage;
	UpdateClientLobbyInfo();
	FOnHostLobbyUpdated.Broadcast(LobbyInfo);
}

void APOVBeaconHostObject::UpdateClientLobbyInfo()
{
	for (AOnlineBeaconClient* ClientBeacon : ClientActors)
	{
		if (APOVBeaconClient* Client = Cast<APOVBeaconClient>(ClientBeacon))
		{
			Client->Client_OnLobbyUpdated(LobbyInfo);
		}
	}
}

void APOVBeaconHostObject::BeginPlay()
{
	LobbyInfo.PlayerList.Add(FString("Host"));
}

void APOVBeaconHostObject::OnClientConnected(AOnlineBeaconClient* NewClientActor, UNetConnection* ClientConnection)
{
	Super::OnClientConnected(NewClientActor, ClientConnection);

		if (NewClientActor)
		{
			FString PlayerName = FString("Player ");
			uint8 Index = LobbyInfo.PlayerList.Num();
			PlayerName.Append(FString::FromInt(Index));
			LobbyInfo.PlayerList.Add(PlayerName);

			if (APOVBeaconClient* Client = Cast<APOVBeaconClient>(NewClientActor))
				Client->SetPlayerIndex(Index);

			UE_LOG(LogTemp, Warning, TEXT("CONNECTED CLIENT VALID"));
			FOnHostLobbyUpdated.Broadcast(LobbyInfo);
			UpdateClientLobbyInfo();
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("CONNECTED CLIENT INVALID"));
		}
}

void APOVBeaconHostObject::NotifyClientDisconnected(AOnlineBeaconClient* LeavingClientActor)
{
	Super::NotifyClientDisconnected(LeavingClientActor);

	UE_LOG(LogTemp, Warning, TEXT("Client Has DISCONNECTED"))
	if (APOVBeaconClient* Client = Cast<APOVBeaconClient>(LeavingClientActor))
	{
		uint8 Index = Client->GetPlayerIndex();
		LobbyInfo.PlayerList.RemoveAt(Index);
	}
	FOnHostLobbyUpdated.Broadcast(LobbyInfo);
	UpdateClientLobbyInfo();
}

void APOVBeaconHostObject::ShutdownServer()
{
	// Unregister server from database via web API
	DisconnectAllClients();

	if (AOnlineBeaconHost* Host = Cast<AOnlineBeaconHost>(GetOwner()))
	{
		UE_LOG(LogTemp, Warning, TEXT("DESTROYING HOST BEACON"));
		Host->UnregisterHost(BeaconTypeName);
		Host->DestroyBeacon();
	}
}


void APOVBeaconHostObject::DisconnectAllClients()
{
	UE_LOG(LogTemp, Warning, TEXT("DISCONNECTING ALL CLIENTS"))
	for (AOnlineBeaconClient* Client : ClientActors)
	{
		if (Client)
			DisconnectClient(Client);
	}
}

void APOVBeaconHostObject::DisconnectClient(AOnlineBeaconClient* ClientActor)
{
	AOnlineBeaconHost* BeaconHost = Cast<AOnlineBeaconHost>(GetOwner());
	if (BeaconHost)
	{
		if (APOVBeaconClient* Client = Cast<APOVBeaconClient>(ClientActor))
		{
			UE_LOG(LogTemp, Warning, TEXT("DISCONNECTING CLIENT %s"), *ClientActor->GetName());
			Client->Client_OnDisconnected();
		}
		BeaconHost->DisconnectClient(ClientActor);
	}
}
