// Fill out your copyright notice in the Description page of Project Settings.


#include "PortalsOfVibration/Public/POV/Mainmenu/POVBeaconClient.h"

APOVBeaconClient::APOVBeaconClient()
{
	PlayerIndex = 0;
}



void APOVBeaconClient::OnFailure()
{
	UE_LOG(LogTemp, Warning, TEXT("CLIENT FAILED TO CONNECT TO HOST BEACON"))
	FOnConnected.Broadcast(false);
}

void APOVBeaconClient::OnConnected()
{
	UE_LOG(LogTemp, Warning, TEXT("CLIENT CONNECTED TO HOST BEACON"))
	FOnConnected.Broadcast(true);
}

void APOVBeaconClient::Client_OnDisconnected_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("DISCONNECTED"));
	FOnDisconnected.Broadcast();
}

void APOVBeaconClient::Client_OnLobbyUpdated_Implementation(FPOVLobbyInfo LobbyInfo)
{
	FOnLobbyUpdated.Broadcast(LobbyInfo);
}

void APOVBeaconClient::SetPlayerIndex(uint8 Index)
{
	PlayerIndex = Index;
}

uint8 APOVBeaconClient::GetPlayerIndex()
{
	return PlayerIndex;
}

bool APOVBeaconClient::ConnectToServer(const FString& Address)
{
	FURL Destination = FURL(nullptr, *Address, ETravelType::TRAVEL_Absolute);
	Destination.Port = 7787;
	return InitClient(Destination);
}

void APOVBeaconClient::LeaveLobby()
{
	DestroyBeacon();
}